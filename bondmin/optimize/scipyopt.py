from scipy.optimize import minimize
from ase.optimize.optimize import Optimizer


class Converged(Exception):
    pass


class MaxedOut(Exception):
    pass


class SP(Optimizer):
    """
    Wrapper for the Scipy optimizers.
    Behaves as a Scipy optimizer but taking atoms object as input.
    """
    def __init__(self, atoms, method='L-BFGS-B', logfile=None,
                 trajectory=None, master=None, force_consistent=None):
        Optimizer.__init__(self, atoms, restart=None,
                           logfile=logfile, trajectory=trajectory,
                           master=master,
                           force_consistent=force_consistent)
        self.method = method

    def run(self, fmax=0.05, steps=100000000):

        self.fmax = fmax
        # Set convergence criterium
        if self.fmax == 'scipy default':
            # Scipys default convergence
            tol = None
        else:
            # ASE's usual behaviour
            tol = 1e-10

        self.max_steps = steps

        f = self.atoms.get_forces()

        self.log(f)
        self.call_observers()
        if self.converged(f):
            return True
        try:
            result = minimize(self.func,
                              self.atoms.positions.ravel(),
                              jac=True,
                              method=self.method,
                              tol=tol)
        except Converged:
            return True
        except MaxedOut:
            return False
        else:
            if result.success is False:
                raise RuntimeError('SciPy Error: ' + str(result.message))
            elif tol is None:
                # Scipy's default convergence was met
                return True

            return False

    def step(self):
        pass

    def converged(self, forces=None):
        if self.fmax == 'scipy default':
            return False
        else:
            return Optimizer.converged(forces)

    def func(self, x):
        self.atoms.set_positions(x.reshape((-1, 3)))
        f = self.atoms.get_forces()
        self.log(f)
        self.call_observers()
        if self.converged(f):
            raise Converged
        self.nsteps += 1
        if self.nsteps == self.max_steps:
            raise MaxedOut

        e = self.atoms.get_potential_energy(
            force_consistent=self.force_consistent)
        return e, -f.ravel()

